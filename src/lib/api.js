import axios from 'axios';
import qs from 'query-string';

const API_KEY = 'AIzaSyAGIE-V1PCJTN_YtCopoRbIN4k7Lu0mAso';

const API_URL = 'https://www.googleapis.com/youtube/v3/search';

export async function getVideos({params}) {
  const {latitude, longitude} = params.location;
  const paramsUrl = qs.stringify(
    {
      key: API_KEY,
      location: [latitude, longitude],
      part: 'snippet',
      locationRadius: '10km',
      type: 'video',
      maxResults: 10,
      order: 'date',
      pageToken: params.pageToken,
    },
    {arrayFormat: 'comma', skipNull: true, skipEmptyString: true},
  );
  const response = await axios.get(`${API_URL}?${paramsUrl}`);

  return {...response.data, isNew: !params.pageToken};
}
