/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, {useState} from 'react';
import {StyleSheet, SafeAreaView} from 'react-native';
import {Provider} from 'react-redux';
import VideoList from './components/VideoList';
import store from './store';
import MapScreen from './components/MapScreen';
const App = () => {
  const [location, setLocation] = useState(null);
  return (
    <Provider store={store}>
      <SafeAreaView style={styles.container}>
        <MapScreen selectedLocation={location} onSelect={setLocation} />
        <VideoList selectedLocation={location} />
      </SafeAreaView>
    </Provider>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
