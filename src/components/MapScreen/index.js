import React, {useRef, useState} from 'react';
import {StyleSheet} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps

const MapScreen = ({selectedLocation, onSelect}) => {
  const mapViewRef = useRef();
  const onSelectPlace = e => {
    const {coordinate} = e.nativeEvent;
    mapViewRef.current.animateCamera({center: coordinate});
    onSelect(coordinate);
  };
  return (
    <MapView
      provider={PROVIDER_GOOGLE} // remove if not using Google Maps
      style={styles.map}
      ref={mapViewRef}
      onPress={onSelectPlace}
      initialRegion={{
        latitude: 41.015137,
        longitude: 28.97953,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
      }}>
      {selectedLocation && <Marker coordinate={selectedLocation} />}
    </MapView>
  );
};

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
});
export default MapScreen;
