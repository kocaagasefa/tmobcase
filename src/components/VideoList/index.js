import React, {useCallback, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  ActivityIndicator,
  Text,
  Button,
} from 'react-native';
import Modal from 'react-native-modal';
import {useDispatch, useSelector} from 'react-redux';
import VideoListItem from '../VideoListItem';
import {getVideosSaga} from '../../store/actions';
import {
  errorSelector,
  nextPageTokenSelector,
  videosLoadingSelector,
  videosSelector,
} from '../../store/selectors';

const VideoList = ({selectedLocation}) => {
  const [isVisible, setIsVisible] = useState(false);
  const videos = useSelector(videosSelector);
  const nextPageToken = useSelector(nextPageTokenSelector);
  const videosLoading = useSelector(videosLoadingSelector);
  const errorMessage = useSelector(errorSelector);

  const dispatch = useDispatch();

  useEffect(() => {
    if (selectedLocation) {
      setIsVisible(true);
      dispatch(getVideosSaga({location: selectedLocation}));
    }
  }, [dispatch, selectedLocation]);

  const getNextPage = useCallback(() => {
    if (nextPageToken && !videosLoading) {
      dispatch(
        getVideosSaga({location: selectedLocation, pageToken: nextPageToken}),
      );
    }
  }, [nextPageToken, dispatch, selectedLocation, videosLoading]);
  const closeModal = () => setIsVisible(false);
  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={closeModal}
      backdropColor={'rgba(255,255,255, .9)'}
      style={styles.modal}>
      <View style={styles.container}>
        <FlatList
          keyExtractor={({id}) => id.videoId}
          ItemSeparatorComponent={renderItemSeparator}
          onEndReached={getNextPage}
          data={videos}
          onEndReachedThreshold={0.001}
          renderItem={({item}) => <VideoListItem video={item} />}
          ListFooterComponent={() => renderFooter(videosLoading)}
          ListHeaderComponent={() => renderHeader(errorMessage)}
        />
        <View style={styles.closeButtonWrapper}>
          <Button title="Kapat" onPress={closeModal} />
        </View>
      </View>
    </Modal>
  );
};

const renderItemSeparator = () => <View style={styles.seperator} />;

const renderFooter = loading =>
  loading ? (
    <View style={styles.loadingWrapper}>
      <ActivityIndicator size="large" color="#0000ff" />
    </View>
  ) : null;

const renderHeader = errorMessage =>
  errorMessage ? (
    <View style={styles.errorWrapper}>
      <Text style={styles.error}>{errorMessage}</Text>
    </View>
  ) : null;

const styles = StyleSheet.create({
  modal: {
    margin: 30,
    marginVertical: 60,
    justifyContent: 'flex-end',
    shadowColor: '#adb5bd',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.35,
    shadowRadius: 23.5,
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    borderColor: '#EEE',
    borderWidth: 2,
  },
  seperator: {
    width: '100%',
    height: 2,
    backgroundColor: '#EEE',
    marginTop: 5,
    shadowColor: '#adb5bd',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.35,
    shadowRadius: 23.5,
  },
  loadingWrapper: {
    justifyContent: 'center',
    flexDirection: 'row',
    paddingVertical: 5,
  },
  errorWrapper: {
    padding: 15,
  },
  error: {
    textAlign: 'center',
    color: '#DD0000',
    fontSize: 16,
  },
  closeButtonWrapper: {
    padding: 15,
  },
});

export default VideoList;
