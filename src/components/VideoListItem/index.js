import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {format} from 'date-fns';

const VideoListItem = ({video}) => {
  return (
    <View style={styles.container}>
      <Image
        source={{uri: video.snippet.thumbnails.default.url}}
        style={styles.image}
      />
      <View style={styles.contentWrapper}>
        <Text style={styles.title}>{video.snippet.title}</Text>
        <Text style={styles.date}>
          {format(new Date(video.snippet.publishedAt), 'yyyy-MM-dd HH:mm')}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 5,
    flexWrap: 'wrap',
    alignItems: 'stretch',
  },
  image: {
    width: 100,
    height: 75,
    marginRight: 10,
    borderRadius: 5,
    resizeMode: 'cover',
  },
  contentWrapper: {
    flex: 1,
    paddingRight: 10,
    justifyContent: 'space-between',
  },
  title: {
    color: '#333',
  },
  date: {
    color: '#888',
  },
});

export default VideoListItem;
