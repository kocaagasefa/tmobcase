export const videosSelector = state => state.videos.list;
export const videosLoadingSelector = state => state.videos.loading;
export const nextPageTokenSelector = state => state.videos.nextPageToken;
export const errorSelector = state => state.videos.error;
