import {
  GET_VIDEOS_FAIL,
  GET_VIDEOS_SAGA,
  GET_VIDEOS_SUCCESS,
} from '../../constants';

export function getVideosSaga(params) {
  return {
    type: GET_VIDEOS_SAGA,
    params,
  };
}

export function getVideosSuccess(videos) {
  return {
    type: GET_VIDEOS_SUCCESS,
    payload: videos,
  };
}

export function getVideosFail(error) {
  return {
    type: GET_VIDEOS_FAIL,
    error,
  };
}
