import {all, fork} from 'redux-saga/effects';

import watchGetVideosSaga from './watchers/getVideos';

export default function* root() {
  yield all([fork(watchGetVideosSaga)]);
}
