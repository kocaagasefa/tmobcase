import {put, takeLatest, call} from 'redux-saga/effects';

import {GET_VIDEOS_SAGA} from '../../../constants';
import {getVideos} from '../../../lib/api';
import {getVideosFail, getVideosSuccess} from '../../actions';

function* workerGetVideosSaga(params) {
  try {
    const videos = yield call(getVideos, params);
    yield put(getVideosSuccess(videos));
  } catch (error) {
    yield put(getVideosFail(error));
  }
}

export default function* watchGetvideosSaga() {
  yield takeLatest(GET_VIDEOS_SAGA, workerGetVideosSaga);
}
