import {
  GET_VIDEOS_FAIL,
  GET_VIDEOS_SAGA,
  GET_VIDEOS_SUCCESS,
} from '../../constants';
const INITIAL_STATE = {
  list: [],
  loading: false,
  error: null,
  nextPageToken: null,
};

const videosReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_VIDEOS_SAGA:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case GET_VIDEOS_SUCCESS:
      return {
        ...state,
        list: action.payload.isNew
          ? action.payload.items
          : [...state.list, ...action.payload.items],
        nextPageToken: action.payload.nextPageToken,
        loading: false,
        error: null,
      };
    case GET_VIDEOS_FAIL:
      return {
        ...state,
        error: action.error.message,
        loading: false,
      };
    default:
      return state;
  }
};

export default videosReducer;
